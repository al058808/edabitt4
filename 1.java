public class WordLengths {
	public static String longestWord(String phrase) {
		String [] split = phrase.split("\\s");
		String longestWord = split[0];
		for(int i = 1 ; i < split.length; i++){
			if(longestWord.length() < split[i].length()){
				longestWord = split[i];
			}
		}
		return longestWord;
	}
}
