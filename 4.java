public class Challenge {
  public static int warOfNumbers(int[]numbers){
		int oddSum = 0;
		int evenSum = 0;    
		
		for (int number : numbers) {
			if (number % 2 == 0)
				evenSum += number;
			else
				oddSum += number;
		}       
		
		return Math.max(oddSum,evenSum) - Math.min(oddSum,evenSum);
  }
}
